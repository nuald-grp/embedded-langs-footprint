#include "proc.hh"
#include <cassert>
#include <memory>
#include <iostream>
#include <string_view>

#include "quickjs-libc.h"

using namespace std;

static JSContext *JS_NewCustomContext(JSRuntime *rt) {
  JSContext *ctx = JS_NewContextRaw(rt);
  if (!ctx) {
    return nullptr;
  }
  JS_AddIntrinsicBaseObjects(ctx);
  JS_AddIntrinsicEval(ctx);
  return ctx;
}

int main(int argc, char **argv) {
  std::shared_ptr<JSRuntime> rt{
    JS_NewRuntime(),
    JS_FreeRuntime
  };

  std::shared_ptr<JSContext> ctx{
    JS_NewCustomContext(rt.get()),
    JS_FreeContext
  };

  static size_t rss;
  // tag::native[]
  JSValue global_obj = JS_GetGlobalObject(ctx.get());
  JS_SetPropertyStr(ctx.get(), global_obj, "read",
    JS_NewCFunction(ctx.get(), [](
        JSContext *ctx, JSValueConst this_val, int argc, JSValueConst *argv) {
      rss = read_rss();
      return JS_NewString(ctx, "world");
    }, "read", 1));
  JS_FreeValue(ctx.get(), global_obj);
  // end::native[]

  const std::string_view src = "const fn = () => 'Hello, ' + read()";
  JSValue val = JS_Eval(ctx.get(), src.data(), src.size(), "<eval>",
    JS_EVAL_TYPE_GLOBAL);
  if (JS_IsException(val)) {
    js_std_dump_error(ctx.get());
  }

  const std::string_view fn_call = "fn()";
  val = JS_Eval(ctx.get(), fn_call.data(), fn_call.size(), "<eval>",
    JS_EVAL_TYPE_GLOBAL);
  if (JS_IsException(val)) {
    js_std_dump_error(ctx.get());
  } else {
    const char *str = JS_ToCString(ctx.get(), val);
    auto value = string(str);
    assert(!value.compare("Hello, world"));

    JS_FreeCString(ctx.get(), str);
  }

  cout << "| QuickJS" << endl;
  cout << "| " << read_size(argv[0]) << endl;
  cout << "| " << rss << endl;
  cout << "| `" << src << "`" << endl;

  return EXIT_SUCCESS;
}
